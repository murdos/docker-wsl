# -*- coding: utf-8 -*-
import argparse
import re
import os

def setup_proxy(http_proxy, https_proxy):
    with open(f"{os.environ['HOME']}/.bashrc", 'r' ) as file :
        filedata = file.read()

    # Replace the target string
    filedata = re.sub('^export HTTP_PROXY=.*?$', '', filedata, flags = re.M)
    filedata = re.sub('^export HTTPS_PROXY=.*?$', '', filedata, flags = re.M)
    filedata = re.sub('^.*Acquire::http::Proxy.*?$', '', filedata, flags = re.M)
    filedata = re.sub('^.*Acquire::https::Proxy.*?$', '', filedata, flags = re.M)
    filedata = re.sub('dockerd', '', filedata, flags = re.M)
    filedata += f"\nexport HTTP_PROXY={http_proxy}"
    filedata += f"\nexport HTTPS_PROXY={https_proxy}"
    filedata += f"""\necho 'Acquire::http::Proxy "'{http_proxy}'";' > /etc/apt/apt.conf.d/proxy.conf"""
    filedata += f"""\necho 'Acquire::https::Proxy "'{https_proxy}'";' >> /etc/apt/apt.conf.d/proxy.conf"""
    filedata += "\ndockerd\n"

    # Write the file out again
    with open(f"{os.environ['HOME']}/.bashrc", 'w') as file:
        file.write(filedata)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Setup proxy configuration for docker in WSL. A least --proxy, --local-proxy or both --http-proxy and --http-proxy should be defined')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-p, --proxy', dest='proxy', metavar='http://user:password@proxy.server:port/', type=str, help='URL of the proxy both HTTP and HTTPS')
    group.add_argument('-l, --local-proxy', dest='local_proxy', help='Specify port to proxy to Windows host as a proxy (i.e. proxy = 127.0.0.1 or localhost on Windows)', type=int)
    group.add_argument('--http-proxy', dest='http_proxy', metavar='http://user:password@proxy.server:port/', type=str, help='URL of the http proxy')
    parser.add_argument('--https-proxy', dest='https_proxy', metavar='http://user:password@proxy.server:port/', type=str, help='URL of the https proxy')

    args = parser.parse_args()
    if args.https_proxy is not None:
        if args.proxy is not None:
            print("argument --https-proxy: not allowed with argument -p, --proxy")
        elif args.local_proxy is not None:
            print("argument --https-proxy: not allowed with argument -l, --local-proxy")
        else:
            setup_proxy(args.http_proxy, args.https_proxy)
    elif args.http_proxy is not None:
        print("--http-proxy and --https-proxy should be used together")
    elif args.proxy is not None:
        setup_proxy(args.proxy, args.proxy)
    elif args.local_proxy is not None:
        setup_proxy(f"""http://$(sed -n "s/^nameserver \(.*\)$/\\1/p" /etc/resolv.conf):{args.local_proxy}/""", f"""http://$(sed -n "s/^nameserver \(.*\)$/\\1/p" /etc/resolv.conf):{args.local_proxy}/""")
